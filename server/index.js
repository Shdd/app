const { GraphQLServer } = require('graphql-yoga');
const { Prisma } = require('prisma-binding');
const { formatError } = require('apollo-errors');
const express = require('express');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const resolvers = require('./graphql/resolvers/index');
const { SECRET } = require('./graphql/utils');

const serverOptions = {
  port: 8080,
  endpoint: '/api/graphql',
  playground: '/api/playground',
  debug: true,
  formatError,
};

// mongodb://mongo:27017
// mongodb://localhost/test
const sessionOptions = {
  store: new MongoStore({ url: 'mongodb://mongo:27017/test' }),
  secret: SECRET,
  resave: false,
  saveUninitialized: false,
}

const server = new GraphQLServer({
  typeDefs: './graphql/schema/index.graphql',  
  resolvers, 
  context: (req, res) => ({    
    ...req,
    ...res,
    db: new Prisma({
      typeDefs: './graphql/database/generated/graphql-schema/prisma.graphql',
      endpoint: 'http://prisma:4466',
      debug: true,
    }),
    user: {
      id: req.request.session.userId,
    },    
  }),
});

server.express.use(express.static('public'));
server.express.use(session(sessionOptions));
server.express.get(/^((?!api).)*$/, (req, res) => {  
  res.redirect('/');
});

server.start(
  serverOptions,  
  () => console.log('Server is running on localhost:8080')
);