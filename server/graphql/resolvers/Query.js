const { prisma } = require('../database/generated/prisma-client');
const { UnauthorizedError } = require('./errors/index');
const getFilteredTasks = require('./utils/getFilteredTasks');

const Query = {
  test: function(obj, args, context, info) {
    return 'Test'
  },
  tasks: async function(obj, args, context, info){
    return await prisma.tasks();
  },
  users: async function(obj, args, context, info){      
    return await prisma.users();
  },
  filterTasks: async function(obj, args, context, info) {
    const { id } = context.user;    
    const { title_contains, tags } = args;

    const user = await prisma.user({ id });

    return getFilteredTasks(user.email, title_contains, tags);
  },
};

module.exports = Query;