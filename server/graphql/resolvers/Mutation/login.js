const { prisma } = require('../../database/generated/prisma-client');
const bcrypt = require('bcryptjs');
const { NoSuchUserError } = require('../errors/index');

async function login(obj, args, context, info) {
  const { email, password } = args;

    const user = await prisma.user({ email });
    if(!user) {
      throw new NoSuchUserError();
    }

    const valid = await bcrypt.compare(password, user.password);
    if(!valid) {
      throw new NoSuchUserError();
    }
    
    context.request.session.userId = user.id;
    
    return user;
}

module.exports = login;
