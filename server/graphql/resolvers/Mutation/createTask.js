const { prisma } = require('../../database/generated/prisma-client');
const getFilteredTasks = require('../utils/getFilteredTasks');

async function createTask(obj, args, context, info) {
  const { title, datetime, duration, venue, description, tags, title_contains, filter_tags } = args;
  
  const user = await prisma.user({ id: context.user.id });  
  
  console.log(args);
  
  for await(const tag of tags) {
    const tagExists = await prisma.$exists.tag({ tagname: tag.trim() });
    
    if(!tagExists) {
      await prisma.createTag({ 
        tagname: tag.trim(),
      });
    }
  } 
  
  const tagsToConnect = tags.map(tag => (
    { tagname: tag }
  ));  
  
  await prisma.createTask({
    title, datetime, duration, venue, description,
    createdBy: {
      connect: {
        email: user.email,
      },
    },
    tags: {
      connect: tagsToConnect,
    }
  });
  
  return getFilteredTasks(user.email, title_contains, filter_tags)
}

module.exports = createTask;