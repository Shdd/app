const { prisma } = require('../../database/generated/prisma-client');
const getFilteredTasks = require('../utils/getFilteredTasks');

async function deleteTask(obj, args, context, info) {
  const { id, title_contains, filter_tags } = args;

  const user = await prisma.user({ id: context.user.id });
  
  await prisma.deleteTask({ id });

  return getFilteredTasks(user.email, title_contains, filter_tags);        
};

module.exports = deleteTask;