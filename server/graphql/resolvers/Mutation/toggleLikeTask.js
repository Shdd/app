const { prisma } = require('../../database/generated/prisma-client');

async function toggleLikeTask(obj, args, context, info) {
  const { id, liked } = args;

  return prisma.updateTask({
    where: { id },
    data: { liked: !liked }
  });
}

module.exports = toggleLikeTask;