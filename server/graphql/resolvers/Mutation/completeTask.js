const { prisma } = require('../../database/generated/prisma-client');
const getFilteredTasks = require('../utils/getFilteredTasks');

async function completeTask(obj, args, context, info) {
  const { id, title_contains, filter_tags } = args;

  await prisma.updateTask({ 
    where: { id },
    data: { completed: true }, 
  });

  const user = await prisma.user({ id: context.user.id });

  return getFilteredTasks(user.email, title_contains, filter_tags);
};

module.exports = completeTask;