async function logout(obj, args, context, info) {
  context.request.session.destroy();
};

module.exports = logout;