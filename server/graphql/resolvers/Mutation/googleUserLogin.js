const { OAuth2Client } = require('google-auth-library');
const { prisma } = require('../../database/generated/prisma-client/');
const getFilteredTasks = require('../utils/getFilteredTasks');

const CLIENT_ID = '283526779859-e78r16u8u7b4jrvrknk3ml0b8pb67j8k.apps.googleusercontent.com';
const client = new OAuth2Client(CLIENT_ID);

async function googleUserLogin(obj, args, context, info) {
  const { id_token } = args;  

  const ticket = await client.verifyIdToken({
    idToken: id_token,
    audience: CLIENT_ID,
  });

  const payload = ticket.getPayload();

  const userExists = await prisma.$exists.user({
    email: payload.email,
  });

  if(!userExists) {
    const user = await prisma.createUser({
      name: payload.name,
      email: payload.email,
      img_url: payload.picture,
    });

    context.request.session.userId = user.id;
    return user;    
  }

  const user = await prisma.user({ email: payload.email });
  const tasks = await getFilteredTasks(user.id, '', []);

  user.tasks = tasks;
  
  context.request.session.userId = user.id;

  return user;
}

module.exports = googleUserLogin;