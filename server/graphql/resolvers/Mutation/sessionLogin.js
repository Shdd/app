const { prisma } = require('../../database/generated/prisma-client');

async function sessionLogin(obj, args, context, info) {
  if(!context.user.id) {
    return null;
  }
  const user = await prisma.user({ id: context.user.id });  

  return user;
}

module.exports = sessionLogin;