const { prisma } = require('../../database/generated/prisma-client');
const bcrypt = require('bcryptjs');
const { EmailExistsError } = require('../errors/index');

async function signup(obj, args, context, info) {
  const { name, email } = args;

  const emailExists = await prisma.$exists.user({ email });
  
  if(emailExists) {
    throw new EmailExistsError();
  }
  
  const password = await bcrypt.hash(args.password, 10);
  const user = await prisma.createUser({ name, email, password });    
  
  context.request.session.userId = user.id;

  return user;
}

module.exports = signup;