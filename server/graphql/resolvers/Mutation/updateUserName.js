const { prisma } = require('../../database/generated/prisma-client');

async function updateUserName(obj, args, context, info) {
  const { id, name } = args;
  const user = await prisma.updateUser({
    where: { id },
    data: { name }
  });

  return user.name;
}

module.exports = updateUserName;