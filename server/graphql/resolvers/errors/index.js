const { createError } = require('apollo-errors');

const UnauthorizedError = createError('UnauthorizedError', {
  message: 'Unauthorized',
  data: {
    code: 401,
  },  
});

const EmailExistsError = createError('EmailExistsError', {
  message: 'This email has already been used',
});

const NoSuchUserError = createError('NoSuchUserError', {
  message: 'No such user',
});

module.exports = {
  UnauthorizedError,
  EmailExistsError,
  NoSuchUserError
};