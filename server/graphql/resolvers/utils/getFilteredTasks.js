const { prisma } = require('../../database/generated/prisma-client');

 async function getFilteredTasks (email, title_contains, tags) {
    const AND = tags.map(tag => (
      {
        tags_some: {
          tagname: tag,
        }
      }
    ));
    
    const where = {
      //  TODO: add completed_not argument to this functoin 
      completed_not: true,
      createdBy: { email },
      title_contains,
      AND,
    };

    return await prisma.tasks({ where });
}

module.exports = getFilteredTasks;

