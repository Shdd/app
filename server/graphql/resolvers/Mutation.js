
const { prisma } = require('../database/generated/prisma-client');

const { EmailExistsError, NoSuchUserError } = require('./errors/index');
const googleUserLogin = require('./Mutation/googleUserLogin');
const createTask = require('./Mutation/createTask');
const getFilteredTasks = require('./utils/getFilteredTasks');
const signup = require('./Mutation/signup');
const login = require('./Mutation/login');
const sessionLogin = require('./Mutation/sessionLogin');
const updateUserName = require('./Mutation/updateUserName');
const logout = require('./Mutation/logout');
const toggleLikeTask = require('./Mutation/toggleLikeTask');
const completeTask = require('./Mutation/completeTask');
const deleteTask = require('./Mutation/deleteTask');
// const uploadUserImage = require('./Mutation/uploadUserImage');

const Mutation = {
  signup,
  login,
  googleUserLogin,
  sessionLogin,
  logout,
  createTask,  
  updateUserName,  
  toggleLikeTask,
  completeTask,  
  deleteTask,
};

module.exports = Mutation;