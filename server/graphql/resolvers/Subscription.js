const { prisma } = require('../database/generated/prisma-client');

async function newArticleSubscribe(parent, args, context, info) {
  // return prisma.$subscribe.article({
  //   where: { mutation_in: ['CREATED', 'UPDATED'] },
  // });
  return context.db.subscription.article({ }, info);
}

const newArticle = {
  subscribe: newArticleSubscribe,
};

module.exports = {
  newArticle,  
};
