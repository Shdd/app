const Query = require('./Query');
const Mutation = require('./Mutation');
const User = require('./User');
const Task = require('./Task');

const resolvers = {
  Query,
  Mutation,
  User,
  Task,
}

module.exports = resolvers;