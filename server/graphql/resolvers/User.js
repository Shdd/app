const { prisma } = require('../database/generated/prisma-client');

const User = { 
  tasks: async function(obj, args, context, info) {
    return await prisma.user({ id: obj.id }).tasks({
      where: {
        completed_not: true,
      },
    });
  },
};

module.exports = User;