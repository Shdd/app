const { prisma } = require('../database/generated/prisma-client');

const Task = {
  createdBy: async function(obj, args, context, info) {
    return await prisma.task({ id: obj.id }).createdBy();      
  },
  tags: async function(obj, args, context, info) {
    return await prisma.task({ id: obj.id }).tags();
  }, 
};

module.exports = Task;

