To run this app you have have Docker, docker-compose, and prisma-cli installed.

1. In the root directory of this app run:
  - docker-compose build && docker-compose up -d
  - cd server/graphql/database
  - docker-compose build && docker-compose up -d
  - prisma deploy

2. run:
  - docker ps

  At this stage you should have 4 docker containers running

4. Run:
  - docker exec -it web_app /bin/bash

5. Inside docker container run:
  - npm install
  - cd app/react-app/ && node build.js

6. In another tab/terminal run:
  - docker exec -it web_app /bin/bash
  - cd app/server && nodemon index.js
  
7. Open another tab/terminal, run:
  - docker exec -it web_app /bin/bash
  - cd app/server
  - chmod +x prefill.sh && ./prefill.sh

App will be running on port 8080

Credentials:
  - email: test@test.com
  - password: pass
