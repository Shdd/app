FROM ubuntu:16.04
# WORKDIR /app
COPY . /app
RUN apt-get update && \
    apt-get install -y curl software-properties-common && \ 
    curl -sL https://deb.nodesource.com/setup_11.x | bash - && \ 
    apt-get install -y nodejs && \
    npm i -g nodemon
EXPOSE 8080
CMD [ "/bin/bash" ]
