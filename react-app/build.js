const webpack = require('webpack');
const path = require('path');

const config = {
  entry: [
    'babel-polyfill',
    './src/index.js',
  ],  
  output: {
    path: path.resolve(__dirname, '../', 'server', 'public'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      { 
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins: [
              'transform-class-properties', 
              ['@babel/plugin-proposal-decorators', { legacy: true }],
            ],
          },
        },
      },
    ],
  },
  watch: true,
  watchOptions: {
    ignored: /node_modules/,
    poll: 500,
  },
  mode: 'development',
};

webpack(config, (error, stats) => {
  if (error) {
    console.log('\n' + error + '\n');
    return;
  }

  console.log(
    '\n' +
    stats.toString({ colors: true, progress: true }) +
    '\n'
  );
});