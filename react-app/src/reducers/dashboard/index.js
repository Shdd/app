import { combineReducers } from 'redux';

import edit_profile from './edit_profile';
import tasks from './tasks';

const dashboard = combineReducers({
  edit_profile,
  tasks,
});

export default dashboard;