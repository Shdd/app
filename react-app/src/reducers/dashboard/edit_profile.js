import { EDIT_PROFILE_ACTIONS } from '../../actions/dashboard/edit_profile';
 
const initialState = {
  file: null,
  filename: '',
}

const edit_profile = (state = initialState, action) => {
  const { payload, type } = action;

  switch(type) {
    case EDIT_PROFILE_ACTIONS.SET_UPLOADED_IMAGE: {
      const { file, filename } = payload;

      return Object.assign({}, state, {
        file,
        filename,
      });
    }

    case EDIT_PROFILE_ACTIONS.EDIT_PROFILE_RESET: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export default edit_profile;