import _ from 'lodash';

import { TASKS_ACTIONS } from '../../actions/dashboard/tasks';

const initialState = {
  modalOpen: false,
  uploadSuccess: false,
  uploadError: false,
  filter_title: '',
  filter_tags: [],
  modal: {
    values: {
      title: '',
      datetime: '2017-05-24T10:30',
      duration: '01:00',
      venue: '',
      description: '',
      tags: [],
    },
    errors: {
      title: null,
      datetime: null,
      duration: null,
      tags: null,
      venue: null,
      description: null,
    },
  },
};

const tasks = (state = initialState, action) => {
  const { type, payload } = action;

  switch(type) {
    case TASKS_ACTIONS.SET_TASKS_MODAL_VISIBLE: {
      const { value } = payload;
      
      return Object.assign({}, state, {
        modalOpen: value,
      });
    }

    case TASKS_ACTIONS.SET_TASKS_MODAL_FIELD_VALUE: {
      const { fieldName, fieldValue } = payload;
      
      const newState = _.cloneDeep(state);
      newState.modal.values[fieldName] = fieldValue;
      return newState;
    }    

    case TASKS_ACTIONS.SET_TASKS_MODAL_FIELD_ERROR: {
      const { fieldName, fieldError } = payload;
      
      const newState = _.cloneDeep(state);
      newState.modal.errors[fieldName] = fieldError;
      return newState;
    }    

    case TASKS_ACTIONS.ADD_TASK: {
      const { task } = payload;

      return Object.assign({}, state, {
        tasks: [...state.tasks, task],
      });
    }

    case TASKS_ACTIONS.SET_UPLOAD_SUCCESS: 
    case TASKS_ACTIONS.SET_UPLOAD_ERROR: {
      const { value } = payload;

      const fieldName = 
        type === 'SET_UPLOAD_SUCCESS' ? 'uploadSuccess' : 'uploadError';

      return Object.assign({}, state, {
        [fieldName]: value,
      });
    }

    case TASKS_ACTIONS.SET_TAGS_FILTER: {
      const { tag } = payload;
      const exists = _.includes(state.filter_tags, tag);
      
      if(exists) {
        const newState = _.cloneDeep(state);
        const newTags = newState.filter_tags.filter(tagItem => {
          if(tagItem !== tag) {
            return tagItem;
          }
        });
        newState.filter_tags = newTags;
        return newState;
      }
      
      return Object.assign({}, state, {
        filter_tags: [...state.filter_tags, tag]        
      }); 
    }

    case TASKS_ACTIONS.SET_TITLE_FILTER: {
      const { substring } = payload;

      return Object.assign({}, state, {
        filter_title: substring,
      });
    }

    case TASKS_ACTIONS.RESET_TASKS_FILTERS: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export default tasks;
