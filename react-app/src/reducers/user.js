import _ from 'lodash';

import { USER_ACTIONS } from '../actions/user';

const initialState = {
  name: '',
  email: '',
  img_url: null,
  tasks: [],
};

const user = (state = initialState, action) => {
  const { type, payload } = action;

  switch(type) {
    case USER_ACTIONS.SET_USER: {
      const { user } = payload;      

      return Object.assign({}, state, {
        ...user,
      });
    }

    case USER_ACTIONS.SET_TASKS: {
      const { tasks } = payload;

      return Object.assign({}, state, {
        tasks,
      });
    }

    case USER_ACTIONS.ADD_TASK: {
      const { task } = payload;

      return Object.assign({}, state, {
        tasks: [...state.tasks, task]
      });
    }

    case USER_ACTIONS.UPDATE_TASK: {
      const { task } = payload;
      
      const newState = _.cloneDeep(state);
      const updatedTasks = state.tasks.map(taskItem => {
        if(taskItem.id === task.id) {
          return task;
        }
        return taskItem;
      });
      newState.tasks = updatedTasks;

      return newState;
    }

    case USER_ACTIONS.SET_USER_NAME: {
      const { name } = payload;
      
      return Object.assign({}, state, {
        name,
      });
    }

    default: {
      return state;
    }
  }
}

export default user;