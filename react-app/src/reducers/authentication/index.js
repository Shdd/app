import { combineReducers } from 'redux';

import signup from './signup';
import login from './login';

const authentication = combineReducers({
  signup,
  login,
});

export default authentication;

