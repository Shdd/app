import _ from 'lodash';

import { LOGIN_ACTIONS } from '../../actions/authentication/login';

const initalState = {  
  values: {
    email: '',
    password: '',
  },
  errors: {
    email: null,
    password: null,
    networkError: null,
  },  
}

const login = (state = initalState, action) => {
  const { type, payload } = action;

  switch(type) {
    case LOGIN_ACTIONS.SET_LOGIN_FIELD_VALUE: {
      const { fieldName, fieldValue } = payload;
      
      const newState = _.cloneDeep(state);
      newState.values[fieldName] = fieldValue;
      return newState;
    }

    case LOGIN_ACTIONS.SET_LOGIN_ERROR_VALUE: {
      const { fieldName, fieldError } = payload;
      
      const newState = _.cloneDeep(state);
      newState.errors[fieldName] = fieldError;
      return newState;
    }

    default: {
      return state
    }
  }
}

export default login;