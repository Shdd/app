import _ from 'lodash';

import { SIGNUP_ACTIONS } from '../../actions/authentication/signup';

const initalState = {  
  values: {
    name: '',
    email: '',
    password: '',
  },
  errors: {
    name: null,
    email: null,
    password: null,
    networkError: null,
  },  
};

const signup = (state = initalState, action) => {
  const { type, payload } = action;

  switch(type) {
    case SIGNUP_ACTIONS.SET_SIGNUP_FIELD_VALUE: {
      const { fieldName, fieldValue } = payload;
      
      const newState = _.cloneDeep(state);
      newState.values[fieldName] = fieldValue;
      return newState;
    }

    case SIGNUP_ACTIONS.SET_SIGNUP_ERROR_VALUE: {
      const { fieldName, fieldError } = payload;
      
      const newState = _.cloneDeep(state);
      newState.errors[fieldName] = fieldError;
      return newState;
    }

    default: {
      return state
    }
  }
}

export default signup;