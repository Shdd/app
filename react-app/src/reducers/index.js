import app from './app';
import authentication from './authentication/index';
import user from './user';
import dashboard from './dashboard/index';

export default {
  app,
  authentication,
  user,
  dashboard,
};