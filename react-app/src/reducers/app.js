import { APP_ACTIONS } from '../actions/app';

const initialState = {  
  isLoading: false,
};

const app = (state = initialState, action) => {
  const { type, payload } = action;

  switch(type) { 

    case APP_ACTIONS.SET_ISLOADING: {
      
      return Object.assign({}, state, {
        isLoading: payload.value,
      });
    }

    default: {
      return state;
    }
  }
}

export default app;