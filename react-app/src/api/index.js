import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createUploadLink } from 'apollo-upload-client'
import gql from 'graphql-tag';

export const client = new ApolloClient({  
  cache: new InMemoryCache(),
  link: createUploadLink({ 
    uri: '/api/graphql',
    credentials: 'include',
  }),
});

export const GOOGLEAPI_CLIENT_ID = '283526779859-e78r16u8u7b4jrvrknk3ml0b8pb67j8k.apps.googleusercontent.com';

const fragments = {};
fragments.task = `
  id
  title
  datetime
  duration
  venue
  description
  img_url
  liked
  tags {
    tagname
  }  
  `;  
fragments.user = `    
  name  
  email  
  about
  img_url  
  tasks {
    ${fragments.task}    
  } 
`;


export const Queries = {
  GET_ARTICLES: gql`
    {
      articles {              
        title
        text
        createdBy {
          name
        }      
      }
    }
  `,
};

export const MUTATION = {
  SIGNUP: gql`
    mutation SignUp($name: String! $email: String! $password: String!) {
      signup(name: $name email: $email password: $password) {      
        ${fragments.user}        
      }
    }
  `,
  GOOGLE_USER_LOGIN: gql`
    mutation GoogleUserLogin($id_token: String!) {
      googleUserLogin(id_token: $id_token) {
        ${fragments.user}
      }
    }
  `,
  SESSION_LOGIN: gql`
    mutation {
      sessionLogin {
        ${fragments.user}
      }
    }
  `,
  LOGIN: gql`
    mutation LogIn($email: String! $password: String!) {
      login(email: $email password: $password) {
        ${fragments.user}
      }
    }
  `,
  LOGOUT: gql`
    mutation {
      logout
    }
  `,
  UPLOAD_USER_IMAGE: gql`
    mutation UploadUserImage($file: Upload!) {
      uploadUserImage(file: $file)
    }
  `,
  CREATE_TASK: gql`
    mutation CreateTask( 
      $title: String! 
      $duration: String! 
      $datetime: String!
      $venue: String
      $tags: [String!]!
      $description: String
      $filter_tags: [String!]
      $title_contains: String
    ) {
      createTask(
        title: $title
        duration: $duration
        datetime: $datetime
        venue: $venue
        tags: $tags
        description: $description
        filter_tags: $filter_tags
        title_contains: $title_contains
      ) {
        ${fragments.task}
      }
    }
  `,
  TOGGLE_LIKE_TASK: gql`
    mutation ToggleLikeTask($id: ID! $liked: Boolean!) {
      toggleLikeTask(id: $id liked: $liked) {
        ${fragments.task}
      }
    }
  `,
  DELETE_TASK: gql`
    mutation DeleteTask($id: ID! $title_contains: String $filter_tags: [String!]) {
      deleteTask(id: $id title_contains: $title_contains filter_tags: $filter_tags) {
        ${fragments.task}
      }
    }
  `,
  COMPLETE_TASK: gql`
    mutation CompleteTask($id: ID! $title_contains: String $filter_tags: [String!]) {
      completeTask(id: $id title_contains: $title_contains filter_tags: $filter_tags) {
        ${fragments.task}
      }
    }
  `,
  UPDATE_USER_NAME: gql`
    mutation UpdateUserName($id: ID! $name: String!) {
      updateUserName(id: $id name: $name)
    }
  `,
};

export const QUERY = {
  FILTER_TASKS: gql`
    query FilterTasks($title_contains: String $tags: [String!]) {
      filterTasks(title_contains: $title_contains tags: $tags) {
        ${fragments.task}
      }
    }
  `,
};
