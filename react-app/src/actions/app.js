/* global gapi */

import generateActionCreator from './utils/generate';
import { MUTATION } from '../api/index';
import { history } from '../components/router';

export const APP_ACTIONS = {  
  SET_ISLOADING: 'SET_ISLOADING',
};

export const setIsLoading = generateActionCreator(APP_ACTIONS.SET_ISLOADING, 'value');
  
export const logout = (client) => (dispatch, getState) => {
  client
    .mutate({
      mutation: MUTATION.LOGOUT,
    })
    .then((response) => {

      gapi.auth2
        .getAuthInstance()
        .signOut()

      history.push('/signup');
    })
    .catch((error) => {
      console.log(error);      
    });
}