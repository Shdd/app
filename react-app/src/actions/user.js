import generateActionCreator from './utils/generate';
import { MUTATION, QUERY } from '../api/index';
import { setIsLoading } from './app';

export const USER_ACTIONS = {
  SET_USER: 'SET_USER', 
  SET_USER_NAME: 'SET_USER_NAME',
  SET_TASKS: 'SET_TASKS',
  ADD_TASK: 'ADD_TASK',
  UPDATE_TASK: 'UPDATE_TASK',  
};

export const setUser = generateActionCreator(USER_ACTIONS.SET_USER, 'user');
export const setTasks = generateActionCreator(USER_ACTIONS.SET_TASKS, 'tasks');
export const addTask = generateActionCreator(USER_ACTIONS.ADD_TASK, 'task');
export const updateTask = generateActionCreator(USER_ACTIONS.UPDATE_TASK, 'task');

export const toggleLikeTask = (id, liked, client) => (dispatch, getState) => {
  
  client
    .mutate({
      mutation: MUTATION.TOGGLE_LIKE_TASK,
      variables: {
        id,
        liked
      }
    })
    .then((response) => {            
      dispatch(updateTask(response.data.toggleLikeTask));
    })
    .catch((error) => {
      console.error(error);
    })
}


export const filterTasks = (client) => (dispatch, getState) => {
  const { tasks } = getState().dashboard;
  
  client
    .query({
      query: QUERY.FILTER_TASKS,
      variables: {
        title_contains: tasks.filter_title,
        tags: tasks.filter_tags,
      }
    })
    .then((response) => {
      dispatch(setTasks(response.data.filterTasks));
    })
    .catch((error) => {
      console.error(error);
    });
  
}

// TODO: handle filterTasks error response


export const deleteTask = (id, client) => (dispatch, getState) => {
  const { filter_title, filter_tags } = getState().dashboard.tasks;
  
  dispatch(setIsLoading(true));

  client
    .mutate({
      mutation: MUTATION.DELETE_TASK,
      variables: {
        id,
        title_contains: filter_title,
        filter_tags: filter_tags,
      },
    })
    .then((response) => {      
      dispatch(setTasks(response.data.deleteTask));      
      dispatch(setIsLoading(false));            
    })
    .catch((error) => {
      console.error(error)
      dispatch(setIsLoading(false));
    })
}

// TODO: handle deleteTask error response

export const completeTask = (id, client) => (dispatch, getState) => {
  const { filter_title, filter_tags } = getState().dashboard.tasks;  
  dispatch(setIsLoading(true));

  client
    .mutate({
      mutation: MUTATION.COMPLETE_TASK,
      variables: {
        id,
        title_contains: filter_title,
        filter_tags: filter_tags,
      },
    })
    .then((response) => {      
      dispatch(setTasks(response.data.completeTask));      
      dispatch(setIsLoading(false));            
    })
    .catch((error) => {
      console.error(error)
      dispatch(setIsLoading(false));
    })
}

export const updateUserName = (client) => (dispatch, getState) => {
  //  TODO
}

