import _ from 'lodash';

import { validateEmail } from './utils/validate';
import { MUTATION } from '../../api/index';
import generateActionCreator from '../utils/generate';
import { setUser } from '../user';
import { setIsLoading } from '../app';
import { history } from '../../components/router'; 

export const LOGIN_ACTIONS = {
  SET_LOGIN_FIELD_VALUE: 'SET_LOGIN_FIELD_VALUE',
  SET_LOGIN_ERROR_VALUE: 'SET_LOGIN_ERROR_VALUE',  
}

export const setLogInFieldValue = generateActionCreator(
  LOGIN_ACTIONS.SET_LOGIN_FIELD_VALUE, 'fieldName', 'fieldValue'
);
/**
 * 
 */
export const setLogInFieldError = generateActionCreator(
  LOGIN_ACTIONS.SET_LOGIN_ERROR_VALUE, 'fieldName', 'fieldError'
);
/**
 * 
 */
export const sessionLogin = (client) => (dispatch, getState) => {
  dispatch(setIsLoading(true));

  client
    .mutate({
      mutation: MUTATION.SESSION_LOGIN,
    })
    .then((response) => {
      if(!response.data.sessionLogin) {
        history.push('/login');
        dispatch(setIsLoading(false));
      } else {
        dispatch(setUser(response.data.sessionLogin));
        history.push('/dashboard/tasks');
        dispatch(setIsLoading(false));
      }      
    })
    .catch((error) => {
      console.log(error);
      dispatch(setIsLoading(false));      
    })
}


export const validateForm = () => (dispatch, getState) => {
  const { values } = getState().authentication.login;
  
  _.forEach(values, (fieldValue, fieldName) => {
    let fieldError = null;

    switch(fieldName) {

      case 'email': {
        if(fieldValue.length === 0) {
          fieldError = 'Field is required'
        } else if (!validateEmail(fieldValue)) {
          fieldError = 'Invalid email address'
        }
        break;
      }

      case 'password': {
        if(fieldValue.length === 0) {
          fieldError = 'Field is required'
        } 
        break;
      }      
      default: break
    }

    dispatch(setLogInFieldError(fieldName, fieldError));
  });
}


export const submitForm = (client) => (dispatch, getState) => {
  dispatch(setLogInFieldError('networkError', null));

  const { values, errors } = getState().authentication.login;

  const hasErrors = _.reduce(errors, (result, fieldError) => (
    result || !_.isNull(fieldError)
  ), false);
  
  if(!hasErrors) {

    client
      .mutate({
        mutation: MUTATION.LOGIN,
        variables: {
          ...values,
        },
      })
      .then((response) => {
        dispatch(setUser(response.data.login));
        history.push('/dashboard/tasks');
      })
      .catch((error) => {
        if(error.graphQLErrors) {
          console.error(error.graphQLErrors)
          dispatch(setLogInFieldError('networkError', error.graphQLErrors[0].message));
        } else {
          console.error(error);
          dispatch(setLogInFieldError('networkError', 'Internal server error'));
        }                
      })
  }
}

export const googleLogin = (client, googleUser) => (dispatch, getState) => { 
  dispatch(setIsLoading(true));

  const id_token = googleUser.getAuthResponse().id_token;
  
  client
    .mutate({
      mutation: MUTATION.GOOGLE_USER_LOGIN,
      variables: {        
        id_token,
      }
    })
    .then((response) => {      
      dispatch(setUser(response.data.googleUserLogin));
      history.push('/dashboard/tasks');
      dispatch(setIsLoading(false));
    })
    .catch((error) => {
      console.error(error);
      dispatch(setIsLoading(false));
    })
}