import _ from 'lodash';

import { validateEmail, validateName } from './utils/validate';
import { MUTATION } from '../../api/index';
import generateActionCreator from '../utils/generate';
import { setUser } from '../user';
import { setIsLoading } from '../app';
import { history } from '../../components/router'; 

export const SIGNUP_ACTIONS = {
  SET_SIGNUP_FIELD_VALUE: 'SET_SIGNUP_FIELD_VALUE',
  SET_SIGNUP_ERROR_VALUE: 'SET_SIGNUP_ERROR_VALUE',  
}

export const setSignUpFieldValue = generateActionCreator(
  SIGNUP_ACTIONS.SET_SIGNUP_FIELD_VALUE, 'fieldName', 'fieldValue'
);
/**
 * 
 */
export const setSignUpFieldError = generateActionCreator(
  SIGNUP_ACTIONS.SET_SIGNUP_ERROR_VALUE, 'fieldName', 'fieldError'
);
/**
 * 
 */
export const validateForm = () => (dispatch, getState) => {
  const { values } = getState().authentication.signup;
  
  _.forEach(values, (fieldValue, fieldName) => {
    let fieldError = null;

    switch(fieldName) {
      case 'name':{
        if(fieldValue.length === 0) {
          fieldError = 'Field is required';
        }
        if(!validateName(fieldValue)) {
          fieldError = 'Invalide name';
        }        
        break;
      }

      case 'email': {
        if(fieldValue.length === 0) {
          fieldError = 'Field is required'
        } else if (!validateEmail(fieldValue)) {
          fieldError = 'Invalid email address'
        }
        break;
      }

      case 'password': {
        if(fieldValue.length === 0) {
          fieldError = 'Field is required'
        } 
        break;
      }      
      default: break
    }

    dispatch(setSignUpFieldError(fieldName, fieldError));
  });
}
/**
 *  
 */
export const submitForm = (client) => (dispatch, getState) => {
  dispatch(setSignUpFieldError('networkError', null));

  const { values, errors } = getState().authentication.signup;

  const hasErrors = _.reduce(errors, (result, fieldError) => (
    result || !_.isNull(fieldError)
  ), false);

  if(!hasErrors) {
    dispatch(setIsLoading(true))

    client
      .mutate({
        mutation: MUTATION.SIGNUP,
        variables: {
          ...values,
        },
      })
      .then((response) => {        
        dispatch(setUser(response.data.signup));
        history.push('/dashboard/tasks');
        dispatch(setIsLoading(false))
      })
      .catch((error) => {
        dispatch(setIsLoading(false));

        if(error.graphQLErrors) {
          console.log(error)
          dispatch(setSignUpFieldError('networkError', error.graphQLErrors[0].message));
        } else {
          console.error(error);
          dispatch(setSignUpFieldError('networkError', 'Internal server error'));
        }                
      })
  }
}