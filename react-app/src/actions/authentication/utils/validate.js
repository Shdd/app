const EMAIL_REGEX = /^[a-z0-9._\-+]+@[a-z0-9.-]+\.[a-z]{2,6}$/i;

export const validateEmail = (email) => EMAIL_REGEX.test(email);

const NAME_REGEX = /^[a-zA-Z\s]+$/;

export const validateName = (name) => (NAME_REGEX.test(name));