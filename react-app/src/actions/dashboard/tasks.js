import _ from 'lodash';

import generateActionCreator from '../utils/generate';
import { MUTATION } from '../../api/index';
import { setIsLoading } from '../../actions/app';
import { setTasks } from '../user';

export const TASKS_ACTIONS = {
  SET_TASKS_MODAL_VISIBLE: 'SET_TASKS_MODAL_VISIBLE',
  SET_TASKS_MODAL_FIELD_VALUE: 'SET_TASKS_MODAL_FIELD_VALUE',
  SET_TASKS_MODAL_FIELD_ERROR: 'SET_TASKS_MODAL_FIELD_ERROR',  
  SET_UPLOAD_SUCCESS: 'SET_UPLOAD_SUCCESS',
  SET_UPLOAD_ERROR: 'SET_UPLOAD_ERROR',
  SET_TITLE_FILTER: 'SET_TITLE_FILTER',
  SET_TAGS_FILTER: 'SET_TAGS_FILTER', 
  RESET_TASKS_FILTERS: 'RESET_TASKS_FILTERS', 
};

export const setTasksModalVisible = generateActionCreator(
  TASKS_ACTIONS.SET_TASKS_MODAL_VISIBLE, 'value'
);

export const setTasksModalFiledValue = generateActionCreator(
  TASKS_ACTIONS.SET_TASKS_MODAL_FIELD_VALUE, 'fieldName', 'fieldValue',
);

export const setTasksModalFiledError = generateActionCreator(
  TASKS_ACTIONS.SET_TASKS_MODAL_FIELD_ERROR, 'fieldName', 'fieldError',
);

export const setUploadSuccess = generateActionCreator(
  TASKS_ACTIONS.SET_UPLOAD_SUCCESS, 'value'
);

export const setUploadError = generateActionCreator(
  TASKS_ACTIONS.SET_UPLOAD_ERROR, 'value'
);

export const setTitleFilter = generateActionCreator(
  TASKS_ACTIONS.SET_TITLE_FILTER, 'substring'
);

export const setTagsFilter = generateActionCreator(
  TASKS_ACTIONS.SET_TAGS_FILTER, 'tag',
);

export const resetTasks = generateActionCreator(
  TASKS_ACTIONS.RESET_TASKS_FILTERS,
)


export const validateTask = () => (dispatch, getState) => {
  const { values } = getState().dashboard.tasks.modal;  

  _.forEach(values, (fieldValue, fieldName) => {
    let fieldError = null;

    switch(fieldName) {

      case 'title': 
      case 'duration':
      case 'datetime': 
      case 'venue': {
        if(fieldValue.trim().length === 0) {
          fieldError = 'Field is required';
        }
        break;
      } 
      case 'tags': {
        if(fieldValue.length === 0) {
          fieldError = 'Field is required';
        } 
        break;
      }      
      default: break
    }

    dispatch(setTasksModalFiledError(fieldName, fieldError));
  });  
}


export const submitTask = (client) => (dispatch, getState) => {
  // dispatch(setLogInFieldError('networkError', null));

  const { tasks } = getState().dashboard;
  const { values, errors } = tasks.modal;
  const { filter_tags, filter_title } = tasks;
  console.error(filter_tags, filter_title);

  const hasErrors = _.reduce(errors, (result, fieldError) => (
    result || !_.isNull(fieldError)
  ), false);
  
  if(!hasErrors) {
    dispatch(setUploadSuccess(false));
    dispatch(setUploadError(false));

    client
      .mutate({
        mutation: MUTATION.CREATE_TASK,
        variables: {
          ...values,
          title_contains: filter_title,
          filter_tags,
        },
      })
      .then((response) => {        
        dispatch(setTasks(response.data.createTask));
        dispatch(setTasksModalVisible(false));
        dispatch(setIsLoading(false));
        dispatch(setUploadSuccess(true));        
      })
      .catch((error) => {
        console.error(error)
        dispatch(setUploadError(true));
      })
  }
}