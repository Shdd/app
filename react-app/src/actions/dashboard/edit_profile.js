import generateActionCreator from '../utils/generate';
import { MUTATION } from '../../api/index';

export const EDIT_PROFILE_ACTIONS = {
  SET_UPLOADED_IMAGE: 'SET_UPLOADED_IMAGE',
  EDIT_PROFILE_RESET: 'EDIT_PROFILE_RESET',
};

export const setUploadedImage = generateActionCreator(
  EDIT_PROFILE_ACTIONS.SET_UPLOADED_IMAGE, 'file', 'filename'
);

export const editProfileReset = generateActionCreator(
  EDIT_PROFILE_ACTIONS.EDIT_PROFILE_RESET,
);

export const uploadFile = (client) => (dispatch, getState) => {
  const { file } = getState().dashboard.edit_profile;
  
  // client.
  //   mutate({
  //     mutation: MUTATION.UPLOAD_USER_IMAGE,
  //     variables: {
  //       file
  //     },
  //   })
  //   .then((response) => {
  //     console.error(response)      
  //   })
  //   .catch((error) => {
  //     console.log(error);      
  //   });
} 
