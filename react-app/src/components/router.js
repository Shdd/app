import React from 'react';
import { Router, Route } from 'react-router-dom';
import { createBrowserHistory} from 'history';

import Dashboard from './Dashboard/index';
import Signup from './Authentication/Signup/index';
import EditProfile from './Dashboard/EditProfile/index';
import Login from './Authentication/Login/index';
import Tasks from './Dashboard/Tasks';

export const history = createBrowserHistory();

const DashboardScreen = (match) => {
  switch(match.params.screen) {
    case 'edit_profile': {
      return <EditProfile />;
    }
    case 'tasks': {
      return <Tasks />;
    }
    default: {
      return null;
    }    
  }
}

export default class AppRouter extends React.Component {
  render() {
    return(
      <Router history={history}>
        <div>          
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/login" component={Login} />
          <Route path="/dashboard/:screen?" render={({ match }) => (
              <Dashboard match={match}>              
                {DashboardScreen(match)}
              </Dashboard>
            )}
          />
        </div>
      </Router>
    )
  }
}