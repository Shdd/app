import React from 'react';
import { connect } from 'react-redux';
import { withApollo } from  'react-apollo';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';


import { AuthenticationWrapper, AuthenticationBody } from '../styles';
import { Margin } from '../../styles';
import { setSignUpFieldValue, validateForm, submitForm } from '../../../actions/authentication/signup';


const mapStateToProps = state => ({
  values: state.authentication.signup.values,
  errors: state.authentication.signup.errors,
});
/**
 * 
 */
const mapDispatchToProps = dispatch => ({  
  setFieldValue: (fieldName, fieldValue) => {
    dispatch(setSignUpFieldValue(fieldName, fieldValue));
  },
  validate: () => {
    dispatch(validateForm());
  },
  submit: (client) => {
    dispatch(submitForm(client));
  },
});
/**
 * 
 */
@withApollo
@connect(mapStateToProps, mapDispatchToProps)
export default class Signup extends React.Component {  
  /**
   * 
   */
  onFieldValueChange = (event) => {
    const { name, value } = event.target;
    this.props.setFieldValue(name, value);
  }
  /**
   * 
   */
  onSignupClick = () => {
    const { validate, submit, client } = this.props;
    
    validate();
    submit(client);
  }
  /**
   *
   */
  render() {

    return (
      <AuthenticationWrapper>
        <Paper>
          <AuthenticationBody>

            <Typography variant="h5" color="textSecondary" align="center" gutterBottom>Sign Up</Typography>
            
            <Margin top={30}>
              <TextField
                fullWidth
                name="name"                 
                variant="outlined" 
                label="Name"
                onChange={this.onFieldValueChange}
                value={this.props.values.name}                
                error={this.props.errors.name && true}
                helperText={this.props.errors.name}                
              />
            </Margin>
            
            <Margin top={30}>
              <TextField 
                fullWidth
                name="email"
                variant="outlined"
                label="Email"
                onChange={this.onFieldValueChange} 
                value={this.props.values.email}
                error={this.props.errors.email && true} 
                helperText={this.props.errors.email} 
              />
            </Margin>

            <Margin top={30}>
              <TextField
                fullWidth 
                name="password"  
                variant="outlined" 
                label="Password" 
                onChange={this.onFieldValueChange}
                value={this.props.values.password}
                error={this.props.errors.password && true}
                helperText={this.props.errors.password} 
              />
            </Margin>
            
            <Margin top={30}>              
              <Button 
                fullWidth 
                variant="contained" 
                color="primary"
                onClick={this.onSignupClick}
              >
                Sign Up
              </Button>
            </Margin>

            <Margin top={15}>              
              <Button
                component={Link}
                to="/login" 
                fullWidth 
                variant="outlined" 
                color="secondary"
                onClick={this.props.onBackClick}
              >
                Back to login
              </Button>                            
            </Margin>

            <Margin top={15}>
              <Typography color="error">{this.props.errors.networkError}</Typography>
            </Margin>
          </AuthenticationBody>
        </Paper>
      </AuthenticationWrapper>
    )
  }
}
