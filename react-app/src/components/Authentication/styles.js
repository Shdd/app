import glamorous from 'glamorous';

export const AuthenticationWrapper = glamorous.div({
  width: 400,
  margin: '0 auto',
  marginTop: 100,
});

export const AuthenticationBody = glamorous.div({
  padding: '20px',
});

export const TextInputIcon = glamorous.div({
  lineHeight: '65px',
  color: 'grey',
});