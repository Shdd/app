/* global gapi */ 

import React from 'react';
import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Lock from '@material-ui/icons/Lock';
import Email from '@material-ui/icons/Email';

import { AuthenticationWrapper, AuthenticationBody, TextInputIcon } from '../styles';
import { Margin } from '../../styles';
import { setLogInFieldValue, validateForm, submitForm, googleLogin } from '../../../actions/authentication/login';

const mapDispatchToProps = dispatch => ({
  validate: () => {
    dispatch(validateForm());
  },
  submit: (client) => {
    dispatch(submitForm(client))
  },
  setFieldValue: (fieldName, fieldValue) => {
    dispatch(setLogInFieldValue(fieldName, fieldValue));
  },
  googleLogin: (client, googleUser) => {
    dispatch(googleLogin(client, googleUser));
  },
});
/**
 * 
 */
const mapStateToProps = state => ({
  values: state.authentication.login.values,
  errors: state.authentication.login.errors,
});


@withApollo
@connect(mapStateToProps, mapDispatchToProps)
export default class Login extends React.Component {

  componentDidMount() {    
    gapi.signin2.render('g-signin2', {
      'scope': 'profile email',
      'width': '360',
      'height': 40,
      'longtitle': true,
      'theme': 'dark',
      'onsuccess': this.onGoogleSignIn,
    });
  }

  onGoogleSignIn = (googleUser) => {    
    this.props.googleLogin(this.props.client, googleUser);    
  }

  onFieldValueChange = (event) => {
    const { name, value } = event.target;
    this.props.setFieldValue(name, value);
  }
  /**
   * 
   */
  onLoginClick = () => {
    const { validate, submit, client } = this.props;
    
    validate();
    submit(client);
  }
  /**
   * 
   */
  render() {
    return (
      <AuthenticationWrapper>
        <Paper>
          <AuthenticationBody>
            
            <Margin bottom={50}>
              <Typography variant="h5" color="textSecondary" align="center" gutterBottom>Log In</Typography>
            </Margin>

            <Margin top={30}>
              <Grid container>
                
                <Grid item xs={1}>
                  <TextInputIcon>
                    <Email />
                  </TextInputIcon>
                </Grid>
                
                <Grid item xs={11}>
                  <TextField 
                    fullWidth 
                    name="email"
                    variant="outlined"
                    placeholder="email"
                    value={this.props.values.email}
                    error={this.props.errors.email && true}
                    helperText={this.props.errors.email}
                    onChange={this.onFieldValueChange}
                  />
                </Grid>

              </Grid>
            </Margin>

            <Margin top={30}>
              <Grid container>

                <Grid item xs={1}>
                  <TextInputIcon>
                    <Lock />
                  </TextInputIcon>
                </Grid>

                <Grid item xs={11}>
                  <TextField 
                    fullWidth
                    type="password"
                    name="password" 
                    variant="outlined" 
                    placeholder="password"
                    value={this.props.values.password}
                    error={this.props.errors.password && true}
                    helperText={this.props.errors.password}
                    onChange={this.onFieldValueChange}
                  />
                </Grid>

              </Grid>
            </Margin>
            
            <Margin top={30}>            
              <Button 
                fullWidth 
                variant="contained" 
                color="primary"
                onClick={this.onLoginClick}           
              >
                Log In
              </Button>
            </Margin>

            <Margin top={10}>
              <Typography align="center">or</Typography>
            </Margin>

            <Margin top={10}>              
              <Button 
                fullWidth
                component={Link}
                to={'/signup'} 
                variant="outlined"
                color="secondary"                
              >
                create an account
              </Button>              
            </Margin>

            <Margin top={15} bottom={15}>
              <Divider />
            </Margin>

            <div id="g-signin2" data-onsuccess="onSignIn"></div>

            <Margin top={15}>
              <Typography color="error">{this.props.errors.networkError}</Typography>
            </Margin>

          </AuthenticationBody>
        </Paper>        
      </AuthenticationWrapper>
    )
  }
}