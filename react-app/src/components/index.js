/* global gapi */

import React from 'react';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
 
import LoadingModal from './LoadingModal/index';
import { theme } from './styles';
import { sessionLogin } from '../actions/authentication/login';
import AppRouter from './router';
import { GOOGLEAPI_CLIENT_ID } from '../api/index';


const mapStateToProps = state => ({  
  isLoading: state.app.isLoading,
});


const mapDispatchToProps = dispatch => ({
  sessionLogin: client => {
    dispatch(sessionLogin(client));
  },
});


@withApollo
@connect(mapStateToProps, mapDispatchToProps)
export default class App extends React.Component {

  componentDidMount() {

    window.gapi.load('auth2', () => {
      window.gapi.auth2.init({
        client_id: GOOGLEAPI_CLIENT_ID,        
      });
    });

    const { sessionLogin, client } = this.props;    
    sessionLogin(client);
  }

  render() {    
    return (      
      <div>
        <MuiThemeProvider theme={theme}>        
          <CssBaseline />
          {this.props.isLoading ? <LoadingModal /> : null}          
          <AppRouter />
        </MuiThemeProvider>
      </div>
    );
  }
}
