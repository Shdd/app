import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import DashboardIcon from '@material-ui/icons/Dashboard';
import Identity from '@material-ui/icons/PermIdentity';
import DateRange from '@material-ui/icons/DateRange';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Avatar from '@material-ui/core/Avatar';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { styles, DashboardBody } from './styles';
import { logout } from '../../actions/app';

const mapDispatchToProps = dispatch => ({
  logout: (client) => {
    dispatch(logout(client));
  },
});


const mapStateToProps = state => ({
  user: state.user,
});


@withApollo
@withStyles(styles)
@connect(mapStateToProps, mapDispatchToProps)
export default class Dashboard extends React.Component {
  
  state = {
    open: true,
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  onLogoutClick = () => {
    const { logout, client } = this.props;
    logout(client);
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>        
        <div className={classes.root}>
          <AppBar
            position="absolute"
            className={classNames(classes.appBar, this.state.open && classes.appBarShift)}
          >
            <Toolbar disableGutters={!this.state.open} className={classes.toolbar}>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={this.handleDrawerOpen}
                className={classNames(
                  classes.menuButton,
                  this.state.open && classes.menuButtonHidden,
                )}
              >
                <MenuIcon />
              </IconButton>
              <Typography
                component="h1"
                variant="h6"
                color="inherit"
                noWrap
                className={classes.title}
              >
                {this.props.user.name}
              </Typography>
              <IconButton color="inherit" onClick={this.onLogoutClick}>
                <ExitToApp />
              </IconButton>
            </Toolbar>
          </AppBar>
          <Drawer
            variant="permanent"
            classes={{
              paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
            }}
            open={this.state.open}
          >
            <div className={classes.toolbarIcon}>
              <IconButton onClick={this.handleDrawerClose}>
                <ChevronLeftIcon />
              </IconButton>
            </div>
            
            <Avatar 
              className={classNames(classes.avatar, classes.bigAvatar)}
              src={this.props.user.img_url}  
            >
              {
                !this.props.user.img_url 
                  ? this.props.user.name[0].toUpperCase()
                  : null
              }
            </Avatar>
            

            <List>
              
              <ListItem 
                button
                component={Link}
                to="/dashboard/edit_profile" 
                selected={this.props.match.params.screen === 'edit_profile' && true}
              >
                <ListItemIcon>
                  <Identity style={{color: '#2196f3'}}/>
                </ListItemIcon>
                <ListItemText primary="Edit Profile" />
              </ListItem>
              
              <ListItem 
                button
                component={Link}
                to="/dashboard/tasks"
                selected={this.props.match.params.screen === 'tasks' && true}                  
              >
                <ListItemIcon>
                  <DateRange style={{color: '#ff5722'}} />
                </ListItemIcon>
                <ListItemText primary="Tasks" />
              </ListItem>
              

            </List>
          </Drawer>

          <main className={classes.content}>
            <DashboardBody>
              {this.props.children}
            </DashboardBody>
          </main>
        </div>
      </React.Fragment>
    );
  }
}