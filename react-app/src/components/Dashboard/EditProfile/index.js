import React from 'react';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import PublishIcon from '@material-ui/icons/Publish';
import Divider from '@material-ui/core/Divider';


import { styles, ChangeAvatarWrapper, EditProfileWrapper, UserInfoContainer } from './styles';
import { Padding, Margin } from '../../styles';
import { setUploadedImage, editProfileReset, uploadFile } from '../../../actions/dashboard/edit_profile';

const mapStateToProps = state => ({
  user: state.user,
  file: state.dashboard.edit_profile.file
});

const mapDispatchToProps = dispatch => ({
  onFileUpload: (e) => {
    const { value, files } = e.target;

    const filename = value.split('\\');    

    dispatch(setUploadedImage(files[0], filename[filename.length - 1]))
  },
  uploadFile: (client) => {
    dispatch(uploadFile(client));
  },
  editProfileReset: () => {
    dispatch(editProfileReset());
  },
});


@withApollo
@connect(mapStateToProps, mapDispatchToProps)
export default class EditProfile extends React.Component {
  
  componentWillUnmount() {
    this.props.editProfileReset();
  }

  onUploadClick = () => {
    const { client, uploadFile } = this.props;
    uploadFile(client);
  }

  render() {
    return(
        
        <EditProfileWrapper>
          <ChangeAvatarWrapper>
            <Paper square elevation={1}>
              <Padding all={10}>
                <Avatar style={styles.avatar}>
                  {this.props.user.name[0].toUpperCase()}
                </Avatar>

                <Margin top={10}>
                  <input 
                    type="file" 
                    hidden id="button-file-upload"
                    onChange={this.props.onFileUpload} 
                  />

                  <label htmlFor="button-file-upload">
                    <Button
                      component="span"
                      variant="fab" 
                      color="inherit"
                      containerelement="label" 
                      disabled
                    >                    
                      <PublishIcon />
                    </Button>
                  </label>
                                    
                  <Button
                    component="span" 
                    variant="contained" 
                    color="primary"                    
                    style={styles.uploadButton}
                    // disabled={!this.props.file}
                    disabled
                    onClick={this.onUploadClick}
                  >
                    Upload
                  </Button>
                  

                </Margin>
              </Padding>
            </Paper>
          </ChangeAvatarWrapper>
          
          <UserInfoContainer>
            <Paper square elevation={1}>
              <Padding all={15}>
                
                <Typography variant="h6" gutterBottom align="center">Profile Info</Typography>

                <TextField 
                  fullWidth    
                  variant="filled"              
                  label="name"    
                  value={this.props.user.name}              
                />

                <Margin top={10}>
                  <Button 
                    variant="contained" 
                    color="primary"
                    size="small"
                    disabled
                  >
                    Update
                  </Button>
                </Margin>
                

                <Margin top={20} bottom={20}>
                  <Divider />
                </Margin>

              </Padding>
            </Paper>
          </UserInfoContainer>          

        </EditProfileWrapper>
    )
  }
}
