import glamorous from 'glamorous';

export const EditProfileWrapper = glamorous.div({
  width: 600,
  margin: '0 auto',
});

export const ChangeAvatarWrapper = glamorous.div({
  width: 200,  
  display: 'inline-block',
});

export const UserInfoContainer = glamorous.div({
  width: 380,
  marginLeft: '10px',  
  verticalAlign: 'top',
  display: 'inline-block',
});

export const styles = {
  avatar: {
    width: 80,
    height: 80,
    margin: '0 auto',    
  },
  uploadButton: {
    marginLeft: '10px'
  },
};