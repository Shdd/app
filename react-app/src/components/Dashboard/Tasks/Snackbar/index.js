import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Button from '@material-ui/core/Button';

import { styles } from './styles'; 

export default class CustomSnackbar extends React.Component {
  render() {
    return(
      <Snackbar
          style={styles.snackbar}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">Task added</span>}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          open={this.props.open}
          autoHideDuration={6000}
          onClose={this.props.onClose}          
        >
          <SnackbarContent      
            style={this.props.variant === 'success' ? styles.success : styles.error}      
            aria-describedby="client-snackbar"
            message={
              <span id="client-snackbar">
                {this.props.variant === 'success' 
                  ? 'New Task added successfuly' 
                  : 'An error occured while creating new task. Try again'
                }                
              </span>
            }
            action={[
              <Button
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={this.props.onClose}
              >
                close
              </Button>,
            ]}
          >
            
          </SnackbarContent>
        </Snackbar>
    )
  }
}

