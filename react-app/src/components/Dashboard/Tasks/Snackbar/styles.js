export const styles = {
  snackbar: {
    marginTop: '50px',
  },
  success: {
    backgroundColor: '#2e7d32',
  },
  error: {
    backgroundColor: '#c62828',
  },
};