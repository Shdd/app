export const styles = {
  cardMedia: {
    height: '150px',
  },
  liked: {
    color: '#2196f3',
  },
  chip: {
    marginRight: 3,
    marginTop: 3,
  },
};

export const themeStyles = theme => ({
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
});