import React from 'react';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';

import { toggleLikeTask, deleteTask, completeTask } from '../../../../actions/user';
import { Margin, Padding } from '../../../styles';
import { styles, themeStyles } from './styles';


const mapDispatchToProps = dispatch => ({
  toggleLikeTask: (id, liked, client) => {
    dispatch(toggleLikeTask(id, liked, client))
  },
  deleteTask: (id, client) => {
    dispatch(deleteTask(id, client));
  },
  completeTask: (id, client) => {
    dispatch(completeTask(id, client));
  },  
});

@withApollo
@connect(null, mapDispatchToProps)
@withStyles(themeStyles)
export default class TaskItem extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      expanded: false,
    };
  }

  handleExpandClick = () => {
    this.setState({
      expanded: !this.state.expanded
    });
  };

  onLikeClick = () => {
    const { id, liked, client, toggleLikeTask } = this.props;
    toggleLikeTask(id, liked, client); 
  }

  onDeleteClick = () => {
    const { id, client, deleteTask } = this.props;
    deleteTask(id, client);
  }

  onCompleteClick = () => {
    const { id, client, completeTask } = this.props;
    completeTask(id, client);
  }

  render() {
    const { classes } = this.props;
    
    return(
        <Grid item xs={6}>
          <Padding all={10}>
            <Margin top={10}>
              <Card square>
                
                <CardHeader
                  action={
                    <Tooltip title="Delete this task">
                      <IconButton onClick={this.onDeleteClick}>
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  }
                  title={this.props.title}
                  subheader={this.props.event_time}
                />            
                
                <CardMedia
                  style={styles.cardMedia}              
                  image={this.props.img_url}              
                />

                <CardContent>

                  {this.props.tags.map((tag, index) => (
                    <Chip                      
                      key={tag.tagname + '_' + index}
                      style={styles.chip}
                      label={tag.tagname}
                      color={tag.tagname === 'Important' ? 'secondary' : 'primary'}
                    />
                  ))}

                  <Margin top={10}>
                    <Grid container>
                      <Grid item xs={6}>
                        <Padding all={5}>
                          <Typography variant="title">Venue</Typography>
                          <Typography>{this.props.venue}</Typography>
                        </Padding>
                      </Grid>

                      <Grid item xs={6}>
                        <Padding all={5}>
                          <Typography variant="title">Duration</Typography>
                          <Typography>{this.props.duration}</Typography>
                        </Padding>
                      </Grid>
                    </Grid>
                  </Margin>
                  
                </CardContent>

                <CardActions className={classes.actions} disableActionSpacing>
                  <IconButton aria-label="Add to favorites" onClick={this.onLikeClick}>
                    <FavoriteIcon style={this.props.liked ? styles.liked : null} />
                  </IconButton>              
                  <IconButton
                    className={classnames(classes.expand, {
                      [classes.expandOpen]: this.state.expanded,
                    })}
                    onClick={this.handleExpandClick}
                    aria-expanded={this.state.expanded}
                    aria-label="Show more"
                  >
                    <ExpandMoreIcon />
                  </IconButton>
                </CardActions>

                <Collapse in={this.state.expanded}>
                  <CardContent>
                    <Typography paragraph>
                      {this.props.description}
                    </Typography>
                  </CardContent>
                  
                  <CardActions>
                    <Padding bottom={10}>
                      <Button 
                        onClick={this.onCompleteClick}
                        color="primary"
                        variant="contained"
                      >
                        Mark as completed 
                      </Button>
                    </Padding>                    
                  </CardActions>
                  
                </Collapse>

              </Card>
            </Margin>
          </Padding>
        </Grid>
    )
  }
}