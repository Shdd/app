import React from 'react';
import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Edit from '@material-ui/icons/Edit';
import TextInput from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Search from '@material-ui/icons/Search';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import { Padding, Margin } from '../../../styles';
import { setTasksModalVisible } from '../../../../actions/dashboard/tasks';
import { setTitleFilter, setTagsFilter } from '../../../../actions/dashboard/tasks';
import { filterTasks } from '../../../../actions/user';
import { tags } from '../TaskModal/utils';

const mapStateToProps = state => ({
  tasks: state.dashboard.tasks,
  filter_tags: state.dashboard.tasks.filter_tags,
});


const mapDispatchToProps = dispatch => ({
  setTasksModalVisible: () => {
    dispatch(setTasksModalVisible(true));
  },
  onFilterChange: (value, client) => {
    dispatch(setTitleFilter(value));
    dispatch(filterTasks(client));
  },
  setTagsFilter: (tag, client) => {
    dispatch(setTagsFilter(tag));
    dispatch(filterTasks(client));
  },
});

@withApollo
@connect(mapStateToProps, mapDispatchToProps)
export default class TasksHeader extends React.Component {
  
  onFilterChange = (event) => {
    const { value } = event.target;
    this.props.onFilterChange(value, this.props.client);
  }

  render() {
    return( 
      <Padding all={10}>
        <Paper square style={{width: 550}}>
          <Padding all={30}>
          
          <Grid container>

            <Grid item xs={9}>              
              <TextInput                   
                variant="outlined"  
                placeholder="Filter tasks by title"
                value={this.props.tasks.filter_title}
                onChange={this.onFilterChange}        
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Search />
                    </InputAdornment>
                  ),
                }}
              />              
            </Grid>        

            <Grid item xs={3}>  
              <Button 
                variant="fab" 
                color="secondary"
                onClick={this.props.setTasksModalVisible}
              >          
                <Edit />                    
              </Button>
            </Grid>
          
          </Grid>        
          
          <Margin top={20} bottom={10}>
            <Divider />
          </Margin>

          <Typography>Filter by tags</Typography>

          <Margin top={10}>
            {tags.map((tag, index) => (
              <FormControlLabel
                key={tag+"_"+index} 
                control={
                  <Checkbox                    
                    value={tag}  
                    onChange={() => { 
                      this.props.setTagsFilter(tag, this.props.client); 
                    }}
                    color="primary"              
                  />
                }
                label={tag}
              />              
            ))}
          </Margin>

          </Padding>
        </Paper>
      </Padding> 
    )
  }
}