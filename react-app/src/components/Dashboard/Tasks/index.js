import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';

import { setUploadSuccess, setUploadError, resetTasks } from '../../../actions/dashboard/tasks';
import { filterTasks } from '../../../actions/user';
import TasksHeader from './TasksHeader/index';
import TaskItem from './TaskItem/index';
import TaskModal from './TaskModal/index';
import CustomSnackbar from './Snackbar/index';
import { TasksWrapper, styles } from './styles';


const mapStateToProps = state => ({
  tasks: state.user.tasks,
  uploadSuccess: state.dashboard.tasks.uploadSuccess,
  uploadError: state.dashboard.tasks.uploadError,
});


const mapDispatchToProps = dispatch => ({
  closeSuccessSnackbar: () => {
    dispatch(setUploadSuccess(false));
  },
  closeErrorSnackbar: () => {
    dispatch(setUploadError(false));
  },
  fetchTasks: (client) => {
    dispatch(filterTasks(client))
  },
  reset: () => {
    dispatch(resetTasks());
  },
});

@withApollo
@connect(mapStateToProps, mapDispatchToProps)
export default class Tasks extends React.Component {
  
  componentDidMount() {
    const { fetchTasks, client } = this.props;
    fetchTasks(client);
  }

  componentWillUnmount() {
    this.props.reset();
  }

  render() {
    
    return(     
      <TasksWrapper>
        
        <CustomSnackbar
          variant="success" 
          open={this.props.uploadSuccess} 
          onClose={this.props.closeSuccessSnackbar}
        />

        <CustomSnackbar
          variant="error" 
          open={this.props.uploadError} 
          onClose={this.props.closeErrorSnackbar}
        />

        <TaskModal />
        
        <TasksHeader />

        <Grid container> 
          {this.props.tasks.map((task, index) => 
            <TaskItem {...task} key={task.datetime + "_" + index} />
          )}       
        </Grid>
        
      </TasksWrapper>             
    )
  }
}

