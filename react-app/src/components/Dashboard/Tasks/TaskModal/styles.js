import glamorous from 'glamorous';

export const TaskModalWrapper = glamorous.div({
  width: 500,
  margin: '0 auto',
  marginTop: 100,
});

export const themeStyles = theme => ({
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: theme.spacing.unit / 4,
  },
  formControl: {    
    maxWidth: '100%',
    minWidth: 200,
  },
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

export const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};