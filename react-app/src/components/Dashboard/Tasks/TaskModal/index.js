import React from 'react';
import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText'
import { withStyles } from '@material-ui/core/styles';

import { Margin, Padding } from '../../../styles'; 
import { TaskModalWrapper, themeStyles, MenuProps } from './styles';
import { setTasksModalVisible, setTasksModalFiledValue, validateTask, submitTask } from '../../../../actions/dashboard/tasks';
import { tags } from './utils';

const mapStateToProps = state => ({
  values: state.dashboard.tasks.modal.values,
  errors: state.dashboard.tasks.modal.errors,
  modalOpen: state.dashboard.tasks.modalOpen,
});
/**
 *
 */
const mapDispatchToProps = dispatch => ({
  closeModal: () => {
    dispatch(setTasksModalVisible(false));
  },
  setFieldValue: (fieldName, fieldValue) => {
    dispatch(setTasksModalFiledValue(fieldName, fieldValue));
  },
  validate: () => {
    dispatch(validateTask());
  },
  submit: (client) => {
    dispatch(submitTask(client));
  } 
});
/**
 * 
 */
@withStyles(themeStyles)
@withApollo
@connect(mapStateToProps, mapDispatchToProps)
export default class TaskModal extends React.Component {
  
  onFieldValueChange = (event) => {
    const { name, value } = event.target;    
    this.props.setFieldValue(name, value);
  }

  onSubmitClick = () => {
    const { validate, submit, client } = this.props;
    validate();
    submit(client);
  }

  render() {
    
    return(
      <Modal
        scroll="body" 
        open={this.props.modalOpen}
        disableAutoFocus
      >
        <TaskModalWrapper>
          <Paper square>
            <Padding all={20}>
              
              <Margin top={20}>
                <TextField 
                  value={this.props.values.title}
                  error={this.props.errors.title && true}
                  helperText={this.props.errors.title}
                  onChange={this.onFieldValueChange}
                  name="title"
                  fullWidth
                  label="Title of event"
                />
              </Margin>

              <Grid container>
                  
                  <Grid item xs={6}>
                    <Margin top={20}>
                      <TextField
                        value={this.props.values.datetime}
                        error={this.props.errors.datetime && true}
                        helperText={this.props.errors.datetime}
                        onChange={this.onFieldValueChange}
                        name="datetime"
                        id="datetime-local"
                        label="Datetime of event"
                        type="datetime-local"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    </Margin>                  
                  </Grid>
                  
                  <Grid item xs={6}>
                    <Margin top={20}>                    
                      <TextField
                        fullWidth
                        value={this.props.values.duration}
                        error={this.props.errors.duration && true}
                        helperText={this.props.errors.duration}
                        onChange={this.onFieldValueChange}
                        name="duration"
                        id="time"
                        label="Duration"
                        type="time"                        
                        InputLabelProps={{
                          shrink: true,
                        }}
                        inputProps={{
                          step: 300,
                        }}
                      />
                    </Margin>
                  </Grid>
                
              </Grid>

              <Margin top={20} bottom={20}>
                <TextField
                  fullWidth
                  value={this.props.values.vanue}
                  error={this.props.errors.venue && true}
                  helperText={this.props.errors.venue}
                  onChange={this.onFieldValueChange}
                  name="venue"                      
                  label="Venue"
                />
              </Margin>

              <TextField
                value={this.props.values.description}
                error={this.props.errors.description && true}
                onChange={this.onFieldValueChange}
                name="description"
                placeholder="Description"
                fullWidth 
                multiline
                variant="outlined"
              />
              
              <FormControl className={this.props.classes.formControl}>
                <InputLabel htmlFor="select-multiple-chip">Tags</InputLabel>                
                <Select                  
                  multiple
                  value={this.props.values.tags}
                  onChange={this.onFieldValueChange}
                  error={this.props.errors.tags && true}
                  name="tags"                  
                  input={<Input id="select-multiple-chip" />}
                  renderValue={selected => (
                    <div className={this.props.classes.chips}>
                      {selected.map(value => {
                        if(selected.length === 0) {
                          return <em>Placeholder</em>;
                        }
                        return <Chip key={value} label={value} className={this.props.classes.chip} />
                      })}
                    </div>
                  )}
                  MenuProps={MenuProps}                  
                >
                  {tags.map(tag => (
                    <MenuItem key={tag} value={tag}>
                      {tag}
                    </MenuItem>
                  ))}
                </Select>
                <FormHelperText error htmlFor="select-multiple-chip">{this.props.errors.tags}</FormHelperText>
              </FormControl>
              
              <Margin top={20} bottom={20}>
                <Divider />
              </Margin>

              <Grid container>
                <Grid item xs={3}>                  
                  <Button 
                    variant="contained" 
                    color="default" 
                    onClick={this.props.closeModal}
                  >Close</Button>
                </Grid>

                <Grid item xs={2}>                  
                  <Button 
                    variant="contained" 
                    color="primary" 
                    onClick={this.onSubmitClick}
                  >Submit</Button>
                </Grid>
              </Grid>

            </Padding>
          </Paper>
        </TaskModalWrapper>
      </Modal>
    )
  }
}