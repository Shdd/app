import React from 'react';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';

import { Margin } from '../styles';

export default class LoadingModal extends React.Component {
  render() {
    return(
      <Modal open disableAutoFocus>
        <Margin top={'50%'} left={'50%'}> 
          <CircularProgress size={100} />        
        </Margin>
      </Modal>
    )
  }
}