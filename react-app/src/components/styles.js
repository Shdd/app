import glamorous from 'glamorous';
import { createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';


export const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
});


export const Margin = glamorous.div(
  props => {
    if(props.auto) {
      return {
        margin: '0 auto',
      }
    } else {
      return {
        marginTop: props.top,
        marginRight: props.right,
        marginBottom: props.bottom,
        marginLeft: props.left,
      }
    }    
  }
);


export const Padding = glamorous.div(
  props => ({
    paddingTop: props.top || props.all,
    paddingRight: props.right || props.all,
    paddingBottom: props.bottom || props.all,
    paddingLeft: props.left || props.all,
  })
);
