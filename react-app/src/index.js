import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import logger from 'redux-logger'; 
import thunk from 'redux-thunk';
import { ApolloProvider } from 'react-apollo';

import App from './components/index';
import { default as applicationReducers } from './reducers/index';
import { client } from './api/index';

const reducers = combineReducers({
  ...applicationReducers,  
});

const store = createStore(
  reducers,  
  applyMiddleware(
    logger,
    thunk,      
  )  
);

render( 
  <ApolloProvider client={client}>
    <Provider store={store}>
      <App />
    </Provider>
  </ApolloProvider>,
  document.getElementById('root')
);

